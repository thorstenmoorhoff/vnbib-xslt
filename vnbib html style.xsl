<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
	version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<html>
			<head>
				<style type="text/css">
					body {
					font-size:1em;
					font-family: Arial;
					width:70%;
					}
					div.artikel {
					border: 1px solid #cecece;
					padding: 10px;
					margin: 20px;
					}
					div.artikel {
					background-color: #ffffff;
					padding: 10px;
					margin: 20px;
					}
					div.metadaten {
					background-color: #C9D8E8;
					padding: 10px;
					margin: 20px;
					}
					span.meta {
					color: #ffffff;
					}
					div.kasten {
					border: 2px solid #000000;
					width: 20%;
					padding: 10px;
					margin: 20px;
					}
					span.werbung {
					background-color: #ff0000;
					line-height: 2em;
					color: #ffffff;
					}
					img {
					display: block;
					}

				</style>
			</head>
			<body>
				<xsl:for-each select="artikel-liste/artikel">

					<div class="artikel">
						<div class="metadaten">
							<p>
								<span class="meta">METADATEN</span>
							</p>
							<xsl:value-of select="metadaten" />
						</div>

						<xsl:apply-templates select="inhalt" />
					</div>
				</xsl:for-each>
			</body>
		</html>
	</xsl:template>
	<xsl:template match="kasten-grafik">
		<xsl:element name="img">
			<xsl:attribute name="src">
        <xsl:value-of select="normalize-space(.)" />
      </xsl:attribute>
			<xsl:attribute name="width">
        <xsl:text>200</xsl:text>
      </xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="inhalt/text/absatz">
		<p>
			<xsl:value-of select="." />
		</p>
	</xsl:template>
	<xsl:template match="grafik">
		<xsl:element name="img">
			<xsl:attribute name="src">
        <xsl:value-of select="normalize-space(.)" />
      </xsl:attribute>
			<xsl:attribute name="width">
        <xsl:text>200</xsl:text>
      </xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="titel">
		<h2>
			<xsl:value-of select="." />
		</h2>
	</xsl:template>
	<xsl:template match="untertitel">
		<h3>
			<xsl:value-of select="." />
		</h3>
	</xsl:template>
	<xsl:template match="zwischenueberschrift">
		<h5>
			<xsl:value-of select="." />
		</h5>
	</xsl:template>
	<xsl:template match="artikel-pdf">
		<xsl:element name="a">
			<xsl:attribute name="href">
        <xsl:value-of select="normalize-space(.)" />
      </xsl:attribute>
			<xsl:value-of select="." />
		</xsl:element>
	</xsl:template>
	<xsl:template match="kasten">
		<div class="kasten">
			<xsl:copy-of select="." />
		</div>
	</xsl:template>
	<xsl:template match="werbung">
		<span class="werbung">WERBUNG</span>
	</xsl:template>


</xsl:stylesheet>